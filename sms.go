package main

import (
	"encoding/json"
	"log"

	"github.com/GiterLab/aliyun-sms-go-sdk/dysms"
	"github.com/tobyzxj/uuid"
)

var (
	alik, alis = globalCfg.GetString("ali.ak"), globalCfg.GetString("ali.as")
	smsSign    = globalCfg.GetString("ali.sms.sign")
)

func init() {
	dysms.HTTPDebugEnable = true
	dysms.SetACLClient(alik, alis)
}

func sendSMS(h *HookMessage) {
	name := "ali.sms.phones." + h.Receiver
	phones := globalCfg.GetString(name)
	for _, a := range h.Alerts {
		msg := a.toSMS()
		bytes, err := json.Marshal(msg)
		if err != nil {
			log.Println(err)
			continue
		}
		send(phones, msg.code, string(bytes))
	}
}

func send(phones, code, templ string) {
	log.Println(phones, code, templ)
	respSendSms, err := dysms.SendSms(uuid.New(), phones, smsSign, code, templ).DoActionWithException()
	if err != nil {
		log.Println(err)
	} else {
		log.Println("send sms succeed", respSendSms.GetRequestID())
	}
}
