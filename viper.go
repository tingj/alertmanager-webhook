package main

import (
	"fmt"
	"log"

	"github.com/spf13/viper"
)

var globalCfg = loadConf()

func loadConf() *viper.Viper {
	cfg := viper.New()
	cfg.SetConfigName("config")
	cfg.AddConfigPath(".")

	err := cfg.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error reading config file: %s", err))
	}
	sign := cfg.GetString("ali.sms.sign")
	log.Println("echo sms sign:", sign)
	if cfg == nil || sign == "" {
		log.Fatalln("empty config file")
	}
	return cfg
}
