package main

import (
	"flag"
	"log"
	"net/http"

	"github.com/davecgh/go-spew/spew"
	"github.com/gin-gonic/gin"
)

var (
	lastReq       = new(HookMessage)
	inspectStream = make(chan *HookMessage, 1)
)

func main() {
	flag.Parse()

	r := gin.Default()

	r.GET("/", healthzHandler)
	r.POST("/alerts", hookHandler)
	r.GET("/alerts", inspect)

	r.Run(globalCfg.GetString("web.addr"))
}

func healthzHandler(c *gin.Context) {
	c.String(http.StatusOK, "hello world\n")
}

func hookHandler(c *gin.Context) {
	msg := new(HookMessage)
	if err := c.BindJSON(msg); err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"err": err.Error()})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "ok"})
		go func() {
			sendSMS(msg)
			logLastReq(msg)
		}()
	}
}

func inspect(c *gin.Context) {
	select {
	case lastReq = <-inspectStream:
	default:
	}
	if len(lastReq.Alerts) == 0 {
		c.JSON(200, gin.H{"empty": "no msg receive yet, or system too busy"})
		return
	}
	msgs := make([]*smsAli, 3)
	for _, a := range lastReq.Alerts {
		msg := a.toSMS()
		msgs = append(msgs, msg)
	}
	c.JSON(200, gin.H{"hookMsg received": spew.Sdump(lastReq), "sms send": spew.Sdump(msgs)})
}

func logLastReq(h *HookMessage) {
	select {
	case <-inspectStream:
		select { // simple call logLastReq recursive can solve block,but not the logic
		case inspectStream <- h:
		default:
		}
	case inspectStream <- h:
	}
}
