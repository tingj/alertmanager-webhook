package main

var smsCode = globalCfg.GetStringMapString("ali.sms.code")

type (
	// HookMessage is the message we receive from Alertmanager
	HookMessage struct {
		Version           string            `json:"version"`
		GroupKey          string            `json:"groupKey"`
		Status            string            `json:"status"`
		Receiver          string            `json:"receiver"`
		GroupLabels       map[string]string `json:"groupLabels"`
		CommonLabels      map[string]string `json:"commonLabels"`
		CommonAnnotations map[string]string `json:"commonAnnotations"`
		ExternalURL       string            `json:"externalURL"`
		Alerts            []Alert           `json:"alerts"`
	}

	// Alert is a single alert.
	Alert struct {
		Labels      map[string]string `json:"labels"`
		Annotations map[string]string `json:"annotations"`
		StartsAt    string            `json:"startsAt,omitempty"`
		EndsAt      string            `json:"EndsAt,omitempty"`
		Status      string            `json:"status"`
	}
)

type smsAli struct {
	Sname  string `json:"servername"`
	Stype  string `json:"type"`
	Svalue string `json:"value"`
	code   string
}

func (a Alert) toSMS() *smsAli {
	m := new(smsAli)
	//msg.Sname = fmt.Sprintf("%s %s ", alert.Labels["nickname"], strings.Split(alert.Labels["instance"], ":")[0])
	m.Sname = a.Labels["nickname"]
	m.Stype = a.Annotations["msgType"]
	m.Svalue = a.Annotations["value"]
	m.code = smsCode[a.Status]

	return m
}
